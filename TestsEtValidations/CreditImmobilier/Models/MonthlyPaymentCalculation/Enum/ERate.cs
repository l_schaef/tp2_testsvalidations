﻿namespace CreditImmobilier.Models.MonthlyPaymentCalculation
{
    public enum ERate
    {
        GoodRate,
        VeryGoodRate,
        ExcellentRate
    }
}
