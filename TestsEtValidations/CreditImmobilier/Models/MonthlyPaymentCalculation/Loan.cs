﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CreditImmobilier.Models.MonthlyPaymentCalculation
{
    public class Loan
    {
        private double _borrowedCapital;
        private ERate _rateState;
        private int _repaymentPeriodInMonths;
        public double BorrowedCapital
        {
            get
            {
                return _borrowedCapital;
            }
            set
            {
                _borrowedCapital = value;
            }
        }
        public ERate RateState
        {
            get
            {
                return _rateState;
            }
            set
            {
                _rateState = value;
            }
        }
        public int RepaymentPeriodInMonths
        {
            get
            {
                return _repaymentPeriodInMonths;
            }
            set
            {
                _repaymentPeriodInMonths = value;
            }
        }

        public Loan() { }

        public Loan(double borrowedCapital, ERate rateState, int repaymentPeriodInMonths)
        {
            BorrowedCapital = borrowedCapital;
            RateState = rateState;
            RepaymentPeriodInMonths = repaymentPeriodInMonths;
        }

    }

}
