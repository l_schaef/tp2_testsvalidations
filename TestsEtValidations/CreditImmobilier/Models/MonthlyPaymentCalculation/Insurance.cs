﻿using System;

namespace CreditImmobilier.Models.MonthlyPaymentCalculation.ViewModels
{
    public class Insurance
    {
        private double _insuranceRate;
        private double _annualInsuranceAmount;
        private double _totalInsuranceAmount;
        private double _monthlyInsuranceAmount;

        public double InsuranceRate
        {
            get
            {
                return _insuranceRate;
            }
            set
            {
                _insuranceRate = value;
            }
        }
        public double AnnualInsuranceAmount
        {
            get
            {
                return _annualInsuranceAmount;
            }
            set
            {
                _annualInsuranceAmount = value;
            }
        }
        public double TotalInsuranceAmount
        {
            get
            {
                return _totalInsuranceAmount;
            }
            set
            {
                _totalInsuranceAmount = value;
            }
        }
        public double MonthlyInsuranceAmount
        {
            get
            {
                return _monthlyInsuranceAmount;
            }
            set
            {
                _monthlyInsuranceAmount = value;
            }
        }

        public Insurance() { }

        public Insurance(Borrower borrower, Loan loan)
        {
            InsuranceRate = InsuranceRateCalculation(borrower);
            AnnualInsuranceAmount = AnnualInsuranceCalculation(InsuranceRate, loan);
            MonthlyInsuranceAmount = MonthlyInsuranceCalculation(AnnualInsuranceAmount, loan);
            TotalInsuranceAmount = TotalInsuranceCalculation(MonthlyInsuranceAmount, loan);
        }

        public static double InsuranceRateCalculation(Borrower borrower)
        {
            double _insuranceRate = 0.30;
            if (borrower.IsAthletic) _insuranceRate -= 0.05;
            if (borrower.IsSmoker) _insuranceRate += 0.15;
            if (borrower.HasHeartTrouble) _insuranceRate += 0.3;
            if (borrower.IsComputerEngineer) _insuranceRate -= 0.05;
            if (borrower.IsFighterPilot) _insuranceRate += 0.15;

            return Math.Round(_insuranceRate,2);
        }

        public static double AnnualInsuranceCalculation(double insuranceRate, Loan loan)
        {
            double _totalInsurance = loan.BorrowedCapital * (insuranceRate / 100);
            return Math.Round(_totalInsurance,2);
        }

        public static double MonthlyInsuranceCalculation(double annualInsurance, Loan loan)
        {
            double _monthlyInsurance = annualInsurance / 12;
            return Math.Round(_monthlyInsurance,2);
        }

        public static double TotalInsuranceCalculation(double monthlyInsurance, Loan loan)
        {
            double _totalInsurance = monthlyInsurance * loan.RepaymentPeriodInMonths;
            return Math.Round(_totalInsurance,2);
        }
    }
}
