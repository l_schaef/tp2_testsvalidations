﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmobilier.Models.MonthlyPaymentCalculation
{
    public class Borrower
    {
        private bool _isAthletic;
        private bool _isSmoker;
        private bool _hasHeartTrouble;
        private bool _isComputerEngineer;
        private bool _isFighterPilot;
        public bool IsAthletic
        {
            get
            {
                return _isAthletic;
            }
            set
            {
                _isAthletic = value;
            }
        }
        public bool IsSmoker
        {
            get
            {
                return _isSmoker;
            }
            set
            {
                _isSmoker = value;
            }
        }
        public bool HasHeartTrouble
        {
            get
            {
                return _hasHeartTrouble;
            }
            set
            {
                _hasHeartTrouble = value;
            }
        }
        public bool IsComputerEngineer
        {
            get
            {
                return _isComputerEngineer;
            }
            set
            {
                _isComputerEngineer = value;
            }
        }
        public bool IsFighterPilot
        {
            get
            {
                return _isFighterPilot;
            }
            set
            {
                _isFighterPilot = value;
            }
        }

        public Borrower() { }

        public Borrower(bool isAthletic, bool isSmoker, bool hasHeartTrouble, bool isComputerEngineer, bool isFighterPilot)
        {
            IsAthletic = isAthletic;
            IsSmoker = isSmoker;
            HasHeartTrouble = hasHeartTrouble;
            IsComputerEngineer = isComputerEngineer;
            IsFighterPilot = isFighterPilot;
        }
    }
}
