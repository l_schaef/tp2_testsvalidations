﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditImmobilier.Models.MonthlyPaymentCalculation
{
    public class SimulationResult
    {
        public double Rate { get; set; }
        public double MonthlyPayment { get; set; }
        public double TotalInterest { get; set; }
        public double RepaymentCapitalAfterTenYears { get; set; }

        public SimulationResult() { }

        public SimulationResult(Loan loan)
        {
            Rate = CalculatedRate(loan);
            MonthlyPayment = MonthlyPaymentCalculation(Rate, loan);
            TotalInterest = TotalInterestCalculation(MonthlyPayment, loan);
            RepaymentCapitalAfterTenYears = RepaymentCapitalAfterTenYearsCalculation(loan);
        }

        public static double CalculatedRate(Loan loan)
        {
            double _rate = 0;
            switch (loan.RateState)
            {
                case ERate.GoodRate:
                    _rate = CalculatedGoodRateByDuration(loan.RepaymentPeriodInMonths);
                    break;
                case ERate.VeryGoodRate:
                    _rate = CalculatedVeryGoodRateByDuration(loan.RepaymentPeriodInMonths);
                    break;
                case ERate.ExcellentRate:
                    _rate = CalculatedExcellentRateByDuration(loan.RepaymentPeriodInMonths);
                    break;
            }
            return Math.Round(_rate,2);
        }

        public static double CalculatedGoodRateByDuration(int duration)
        {
            double _rate = 1.27;
            switch (duration)
            {
                case < 120:
                    _rate = 0.62;
                    break;
                case < 180:
                    _rate = 0.67;
                    break;
                case < 240:
                    _rate = 0.85;
                    break;
                case < 300:
                    _rate = 1.04;
                    break;
            }
            return _rate;
        }

        public static double CalculatedVeryGoodRateByDuration(int duration)
        {
            double _rate = 1.15;
            switch (duration)
            {
                case < 120:
                    _rate = 0.43;
                    break;
                case < 180:
                    _rate = 0.55;
                    break;
                case < 240:
                    _rate = 0.73;
                    break;
                case < 300:
                    _rate = 0.91;
                    break;
            }
            return _rate;
        }

        public static double CalculatedExcellentRateByDuration(int duration)
        {
            double _rate = 0.89;
            switch (duration)
            {
                case < 120:
                    _rate = 0.35;
                    break;
                case < 180:
                    _rate = 0.45;
                    break;
                case < 240:
                    _rate = 0.58;
                    break;
                case < 300:
                    _rate = 0.73;
                    break;
            }
            return _rate;
        }

        public static double RepaymentCapitalAfterTenYearsCalculation(Loan loan)
        {
            if (loan.RepaymentPeriodInMonths <= 10*12)
            {
                return loan.BorrowedCapital;
            }
            else
            {
                double _repaymentCapitalAfterTenYears = (loan.BorrowedCapital / loan.RepaymentPeriodInMonths) * (10 * 12);
                return Math.Round(_repaymentCapitalAfterTenYears,2);
            }
        }

        public static double MonthlyPaymentCalculation(double calculatedRate, Loan loan)
        {
            double _monthlyRate = (calculatedRate / 100) / 12;
            double _numerateur = loan.BorrowedCapital * _monthlyRate;
            double _denominateur = 1 - (Math.Pow((1 + _monthlyRate), (loan.RepaymentPeriodInMonths * -1)));
            double _monthlyPayment = _numerateur / _denominateur;
            return Math.Round(_monthlyPayment,2);
        }

        public static double TotalInterestCalculation(double monthlyPayment, Loan loan)
        {
            double _totalInterest = (monthlyPayment * loan.RepaymentPeriodInMonths) - loan.BorrowedCapital;
            return Math.Round(_totalInterest,2);
        }
    }
}
