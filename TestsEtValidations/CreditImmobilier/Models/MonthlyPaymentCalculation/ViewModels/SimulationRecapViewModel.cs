﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CreditImmobilier.Models.MonthlyPaymentCalculation.ViewModels;

namespace CreditImmobilier.Models.MonthlyPaymentCalculation
{
    public class SimulationRecapViewModel
    {
        [DisplayName("Montant emprunté")]
        public double BorrowedCapital { get; set; }
        [DisplayName("Durée de remboursement")]
        public int RepaymentPeriodInMonths { get; set; }
        [DisplayName("Taux annuel du crédit")]
        public double Rate { get; set; }
        [DisplayName("Montant de la mensualité")]
        public double MonthlyPayment { get; set; }
        [DisplayName("Montant total des intérêts (coût du crédit)")]
        public double TotalInterest { get; set; }
        [DisplayName("Taux de l'assurance")]
        public double InsuranceRate { get; set; }
        [DisplayName("Montant de l'assurance par mensualité")]
        public double MonthlyInsurance { get; set; }
        [DisplayName("Montant total de l'assurance")]
        public double TotalInsurance { get; set; }
        [DisplayName("Capital remboursé après 10 ans")]
        public double RepaymentCapitalAfterTenYears { get; set; }


        public SimulationRecapViewModel() { }

        public SimulationRecapViewModel(MonthlyPaymentCalculationViewModel monthlyPaymentCalculationViewModel, SimulationResult simulationResult, Insurance insurance)
        {
            BorrowedCapital = monthlyPaymentCalculationViewModel.BorrowedCapital;
            RepaymentPeriodInMonths = monthlyPaymentCalculationViewModel.RepaymentPeriodInMonths;
            Rate = simulationResult.Rate;
            MonthlyPayment = simulationResult.MonthlyPayment;
            TotalInterest = simulationResult.TotalInterest;
            InsuranceRate = insurance.InsuranceRate;
            MonthlyInsurance = insurance.MonthlyInsuranceAmount;
            TotalInsurance = insurance.TotalInsuranceAmount;
            RepaymentCapitalAfterTenYears = simulationResult.RepaymentCapitalAfterTenYears;
        }

    }
}
