﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CreditImmobilier.Models.MonthlyPaymentCalculation
{
    public class MonthlyPaymentCalculationViewModel
    {
        [DisplayName("Sportif")]
        public bool BorrowerIsAthletic { get; set; }
        [DisplayName("Fumeur")]
        public bool BorrowerIsSmoker { get; set; }
        [DisplayName("Troubles cardiques")]
        public bool BorrowerHasHeartTrouble { get; set; }
        [DisplayName("Ingénieur informatique")]
        public bool BorrowerIsComputerEngineer { get; set; }
        [DisplayName("Pilote de chasse")]
        public bool BorrowerIsFighterPilot { get; set; }
        [Required(ErrorMessage = "Montant emprunté requis")]
        [Range(50000, 1000000000000)]
        [DisplayName("Capital emprunté (en €)")]
        public double BorrowedCapital { get; set; }
        [Required(ErrorMessage = "Veuillez sélectionner un niveau de taux")]
        [DisplayName("Taux")]
        public ERate RateState { get; set; }
        [Required(ErrorMessage = "Durée de remboursement requis")]
        [Range(9 * 12, 25 * 12)]
        [DisplayName("Durée de remboursement (en mois)")]
        public int RepaymentPeriodInMonths { get; set; }

        public MonthlyPaymentCalculationViewModel() { }

        public MonthlyPaymentCalculationViewModel(Loan loan, Borrower borrower)
        {
            BorrowerIsAthletic = borrower.IsAthletic;
            BorrowerIsSmoker = borrower.IsSmoker;
            BorrowerHasHeartTrouble = borrower.HasHeartTrouble;
            BorrowerIsComputerEngineer = borrower.IsComputerEngineer;
            BorrowerIsFighterPilot = borrower.IsFighterPilot;
            BorrowedCapital = loan.BorrowedCapital;
            RateState = loan.RateState;
            RepaymentPeriodInMonths = loan.RepaymentPeriodInMonths;
        }
    }
}
