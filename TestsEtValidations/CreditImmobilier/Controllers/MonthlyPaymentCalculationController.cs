﻿using CreditImmobilier.Models.MonthlyPaymentCalculation;
using CreditImmobilier.Models.MonthlyPaymentCalculation.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CreditImmobilier.Controllers
{
    public class MonthlyPaymentCalculationController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "Calcul de mensualités";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Calculation(MonthlyPaymentCalculationViewModel monthlyPaymentCalculationViewModel)
        {
            ViewBag.Title = "Résultat de simulation";

            Loan newLoan = new(monthlyPaymentCalculationViewModel.BorrowedCapital, monthlyPaymentCalculationViewModel.RateState, monthlyPaymentCalculationViewModel.RepaymentPeriodInMonths);
            Borrower newBorrower = new(monthlyPaymentCalculationViewModel.BorrowerIsAthletic, monthlyPaymentCalculationViewModel.BorrowerIsSmoker, monthlyPaymentCalculationViewModel.BorrowerHasHeartTrouble, monthlyPaymentCalculationViewModel.BorrowerIsComputerEngineer, monthlyPaymentCalculationViewModel.BorrowerIsFighterPilot);
            SimulationResult newSimulationResult = new(newLoan);
            Insurance newInsurance = new(newBorrower, newLoan);

            SimulationRecapViewModel simulationRecapViewModel = new(monthlyPaymentCalculationViewModel, newSimulationResult, newInsurance);

            return View("SimulationResult", simulationRecapViewModel);
        }

        
    }
}
