﻿using System;
using CreditImmobilier.Models.MonthlyPaymentCalculation;
using CreditImmobilier.Controllers;
using Xunit;
using CreditImmobilier.Models;
using CreditImmobilier.Models.MonthlyPaymentCalculation.ViewModels;

namespace Tests
{
    public class LoanSimulationTest
    {
        [Fact]
        [Trait("Categorie", "Insurance")]
        public void InsuranceRateCalculation()
        {
            double primeRate = 0.3;
            double athleticRateOperation = 0.05 * (-1);
            double smokerRateOperation = 0.15;
            double troubleHearthRateOperation = 0.3;
            double computerEngineerRateOperation = 0.05 * (-1);
            double fighterPilotRateOperation = 0.15;

            Borrower athleticBorrower = new(true, false, false, false, false);
            double athleticRate = primeRate + athleticRateOperation;
            Assert.Equal(athleticRate, Insurance.InsuranceRateCalculation(athleticBorrower));

            Borrower smokerBorrower = new(false, true, false, false, false);
            double smokerRate = primeRate + smokerRateOperation;
            Assert.Equal(smokerRate, Insurance.InsuranceRateCalculation(smokerBorrower));

            Borrower troubleHearthBorrower = new(false, false, true, false, false);
            var troubleHeartRate = primeRate + troubleHearthRateOperation;
            Assert.Equal(troubleHeartRate, Insurance.InsuranceRateCalculation(troubleHearthBorrower));

            Borrower computerEngineerBorrower = new(false, false, false, true, false);
            double computerEngineerRate = primeRate + computerEngineerRateOperation;
            Assert.Equal(computerEngineerRate, Insurance.InsuranceRateCalculation(computerEngineerBorrower));

            Borrower fighterPilotBorrower = new(false, false, false, false, true);
            double fighterPilotRate = primeRate + fighterPilotRateOperation;
            Assert.Equal(fighterPilotRate, Insurance.InsuranceRateCalculation(fighterPilotBorrower));

            Borrower primeBorrower = new(false, false, false, false, false);
            Assert.Equal(primeRate, Insurance.InsuranceRateCalculation(primeBorrower));

            Borrower completeBorrower = new(true, true, true, true, true);
            double completeRate = primeRate + athleticRateOperation + smokerRateOperation + troubleHearthRateOperation + computerEngineerRateOperation + fighterPilotRateOperation;
            Assert.Equal(completeRate, Insurance.InsuranceRateCalculation(completeBorrower));
        }

        [Fact]
        [Trait("Categorie", "Insurance")]
        public void AnnualInsuranceCalculation()
        {
            Loan loan1 = new(120000, ERate.GoodRate, 7 * 12);
            double athleticInsuranceRate = 0.25;
            double calculatedAnnualAthleticInsurance = 300;
            Assert.Equal(calculatedAnnualAthleticInsurance, Math.Round(Insurance.AnnualInsuranceCalculation(athleticInsuranceRate, loan1),2));

            Loan loan2 = new(100000, ERate.GoodRate, 10 * 12);
            double smokerInsuranceRate = 0.45;
            double calculatedAnnualSmokerInsurance = 450;
            Assert.Equal(calculatedAnnualSmokerInsurance, Math.Round(Insurance.AnnualInsuranceCalculation(smokerInsuranceRate, loan2),2));

            Loan loan3 = new(150000, ERate.GoodRate, 25 * 12);
            double troubleHearthInsuranceRate = 0.6;
            double calculatedAnnualTroubleHeartInsurance = 900;
            Assert.Equal(calculatedAnnualTroubleHeartInsurance, Math.Round(Insurance.AnnualInsuranceCalculation(troubleHearthInsuranceRate, loan3),2));

        }

        [Fact]
        [Trait("Categorie", "Insurance")]
        public void MonthlyInsuranceCalculation()
        {
            Loan loan1 = new(120000, ERate.GoodRate, 7 * 12);
            double annualInsurance1 = 300;
            double monthlyInsurance1 = 25;
            Assert.Equal(Math.Round(monthlyInsurance1, 2), Math.Round(Insurance.MonthlyInsuranceCalculation(annualInsurance1, loan1),2));

            Loan loan2 = new(100000, ERate.GoodRate, 10 * 12);
            double annualInsurance2 = 450;
            double monthlyInsurance2 = 37.5;
            Assert.Equal(Math.Round(monthlyInsurance2, 2), Math.Round(Insurance.MonthlyInsuranceCalculation(annualInsurance2, loan2), 2));

            Loan loan3 = new(150000, ERate.GoodRate, 25 * 12);
            double annualInsurance3 = 900;
            double monthlyInsurance3 = 75;
            Assert.Equal(Math.Round(monthlyInsurance3, 2), Math.Round(Insurance.MonthlyInsuranceCalculation(annualInsurance3, loan3), 2));
        }

        [Fact]
        [Trait("Categorie", "Insurance")]
        public void TotalInsuranceCalculation()
        {
            Loan loan1 = new(120000, ERate.GoodRate, 7 * 12);
            double monthlyInsurance1 = 25;
            double totalInsurance1 = 2100;
            Assert.Equal(Math.Round(totalInsurance1, 2), Math.Round(Insurance.TotalInsuranceCalculation(monthlyInsurance1, loan1), 2));

            Loan loan2 = new(100000, ERate.GoodRate, 10 * 12);
            double monthlyInsurance2 = 37.5;
            double totalInsurance2 = 4500;
            Assert.Equal(Math.Round(totalInsurance2, 2), Math.Round(Insurance.TotalInsuranceCalculation(monthlyInsurance2, loan2), 2));

            Loan loan3 = new(150000, ERate.GoodRate, 25 * 12);
            double monthlyInsurance3 = 75;
            double totalInsurance3 = 22500;
            Assert.Equal(Math.Round(totalInsurance3, 2), Math.Round(Insurance.TotalInsuranceCalculation(monthlyInsurance3, loan3), 2));

        }

        [Fact]
        [Trait("Categorie", "Rate")]
        public void GoodRateByDurationInMonthsCalculation()
        {
            double sevenYearsGoodRate = 0.62;
            double tenYearsGoodRate = 0.67;
            double fifteenYearsGoodRate = 0.85;
            double twentyYearsGoodRate = 1.04;
            double twentyFiveYearsGoodRate = 1.27 ;
           
            Assert.Equal(sevenYearsGoodRate, SimulationResult.CalculatedGoodRateByDuration(7*12));
            Assert.Equal(tenYearsGoodRate, SimulationResult.CalculatedGoodRateByDuration(10 * 12));
            Assert.Equal(fifteenYearsGoodRate, SimulationResult.CalculatedGoodRateByDuration(15 * 12));
            Assert.Equal(twentyYearsGoodRate, SimulationResult.CalculatedGoodRateByDuration(20 * 12));
            Assert.Equal(twentyFiveYearsGoodRate, SimulationResult.CalculatedGoodRateByDuration(25 * 12));
        }

        [Fact]
        [Trait("Categorie", "Rate")]
        public void VeryGoodRateByDurationInMonthsCalculation()
        {
            double sevenYearsVeryGoodRate = 0.43;
            double tenYearsVeryGoodRate = 0.55;
            double fifteenYearsVeryGoodRate = 0.73;
            double twentyYearsVeryGoodRate = 0.91;
            double twentyFiveYearsVeryGoodRate = 1.15;

            Assert.Equal(sevenYearsVeryGoodRate, SimulationResult.CalculatedVeryGoodRateByDuration(7 * 12));
            Assert.Equal(tenYearsVeryGoodRate, SimulationResult.CalculatedVeryGoodRateByDuration(10 * 12));
            Assert.Equal(fifteenYearsVeryGoodRate, SimulationResult.CalculatedVeryGoodRateByDuration(15 * 12));
            Assert.Equal(twentyYearsVeryGoodRate, SimulationResult.CalculatedVeryGoodRateByDuration(20 * 12));
            Assert.Equal(twentyFiveYearsVeryGoodRate, SimulationResult.CalculatedVeryGoodRateByDuration(25 * 12));
        }

        [Fact]

        [Trait("Categorie", "Rate")]
        public void ExcellentRateByDurationInMonthsCalculation()
        {
            double sevenYearsExcellentRate = 0.35;
            double tenYearsExcellentRate = 0.45;
            double fifteenYearsExcellentRate = 0.58;
            double twentyYearsExcellentRate = 0.73;
            double twentyFiveYearsExcellentRate = 0.89;

            Assert.Equal(sevenYearsExcellentRate, SimulationResult.CalculatedExcellentRateByDuration(7 * 12));
            Assert.Equal(tenYearsExcellentRate, SimulationResult.CalculatedExcellentRateByDuration(10 * 12));
            Assert.Equal(fifteenYearsExcellentRate, SimulationResult.CalculatedExcellentRateByDuration(15 * 12));
            Assert.Equal(twentyYearsExcellentRate, SimulationResult.CalculatedExcellentRateByDuration(20 * 12));
            Assert.Equal(twentyFiveYearsExcellentRate, SimulationResult.CalculatedExcellentRateByDuration(25 * 12));
        }

        [Fact]
        [Trait("Categorie", "Loan")]
        public void RateCalculation()
        {
            Loan loan1 = new Loan(0, ERate.GoodRate, 7 * 12);
            Assert.Equal(0.62, SimulationResult.CalculatedRate(loan1));

            Loan loan2 = new Loan(0, ERate.VeryGoodRate, 15 * 12);
            Assert.Equal(0.73, SimulationResult.CalculatedRate(loan2));

            Loan loan3 = new Loan(0, ERate.ExcellentRate, 25 * 12);
            Assert.Equal(0.89, SimulationResult.CalculatedRate(loan3));
        }

        [Fact]
        [Trait("Categorie", "Loan")]
        public void MonthlyPaymentCalculation()
        {
            Loan loan1 = new Loan(125000, ERate.VeryGoodRate, 15 * 12);
            double rate1 = 0.73;
            double monthlyPayment1 = 733.37;
            Assert.Equal(monthlyPayment1, Math.Round(SimulationResult.MonthlyPaymentCalculation(rate1, loan1),2));

            Loan loan2 = new Loan(50000, ERate.GoodRate, 25 * 12);
            double rate2 = 1.27;
            double monthlyPayment2 = 194.61;
            Assert.Equal(monthlyPayment2, Math.Round(SimulationResult.MonthlyPaymentCalculation(rate2, loan2), 2));
        }

        [Fact]
        [Trait("Categorie", "Loan")]
        public void TotalInterestCalculation()
        {
            Loan loan1 = new Loan(125000, ERate.VeryGoodRate, 15 * 12);
            double monthlyPayment1 = 733.37;
            double totalInterest1 = 7006.60;
            Assert.Equal(totalInterest1, Math.Round(SimulationResult.TotalInterestCalculation(monthlyPayment1, loan1), 2));

            Loan loan2 = new Loan(50000, ERate.GoodRate, 25 * 12);
            double monthlyPayment2 = 194.61;
            double totalInterest2 = 8383;
            Assert.Equal(totalInterest2, Math.Round(SimulationResult.TotalInterestCalculation(monthlyPayment2, loan2), 2));
        }

        [Fact]
        [Trait("Categorie", "Loan")]
        public void RepaymentCapitalAfterTenYearsCalculation()
        {
            Loan loan1 = new Loan(100000, ERate.GoodRate, 240);
            double repaymentCapitalAfterTenYears1 = ((double)100000 / 240) * 120;
            Assert.Equal(Math.Round(repaymentCapitalAfterTenYears1,2), Math.Round(SimulationResult.RepaymentCapitalAfterTenYearsCalculation(loan1), 2));

            Loan loan2 = new Loan(300000, ERate.GoodRate, 15 * 12);
            double repaymentCapitalAfterTenYears2 = ((double)300000 / (15 * 12)) * (10 * 12);
            Assert.Equal(Math.Round(repaymentCapitalAfterTenYears2,2), Math.Round(SimulationResult.RepaymentCapitalAfterTenYearsCalculation(loan2), 2));

            Loan loan3 = new Loan(150000, ERate.GoodRate, 10 * 12);
            Assert.Equal(150000, Math.Round(SimulationResult.RepaymentCapitalAfterTenYearsCalculation(loan3), 2));

            Loan loan4 = new Loan(150000, ERate.GoodRate, 7 * 12);
            Assert.Equal(150000, Math.Round(SimulationResult.RepaymentCapitalAfterTenYearsCalculation(loan4), 2));
        }

    }
}
